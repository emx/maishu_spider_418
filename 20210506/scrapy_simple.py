# -*- coding: utf-8 -*-
# @Date    : 2021/5/6
# @Author  : Maoxian

# 安装scrapy步骤
# pip install Twisted==20.3.0
# pip install scrapy==2.5.0

# scrapy 使用：
# 1. 创建项目
# scrapy startproject project_name

# 2. 生成爬虫
# cd project_name
# scrapy genspider spider_name example.com

# 3. 运行爬虫
# scrapy crawl spider_name
