# -*- coding: utf-8 -*-
# @Date    : 2021/5/15
# @Author  : Maoxian
import time
import requests

rs = requests.Session()
rs.headers = {
    "User-Agent": "qiushibalke_11.7.11_WIFI_auto_6",
    "Model": "MI 6_23"
}


def get_joke(page):
    joke_url = f'https://m2.qiushibaike.com/article/list/text?page={page}&count=12'
    res = rs.get(joke_url)
    for item in res.json()['items']:
        create_at = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(item['created_at']))
        content = ''.join(item['content'].split(' ')).replace('\r', '\n').replace('\n\n', '\n')
        with open("joke.txt", "a", encoding="utf8") as f:
            f.write("*" * 60 + "\n")
            f.write(f"作者：{item['user']['login']}\t\t创作时间：{create_at}\n")
            f.write(f"{content}\n")


for i in range(1, 11):
    get_joke(i)
