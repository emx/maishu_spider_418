# -*- coding:utf-8 -*-
# @Date      :2021/5/1
# @Author    :Maoxian

# 一些免费代理网站
# https://www.kuaidaili.com/free/
# https://ip.jiangxianli.com/
# https://www.89ip.cn/

import requests

proxy = {
    "http": "http://122.138.155.172:9999"
}

res = requests.get('http://httpbin.org/get', proxies=proxy)
print(res.json())
