# -*- coding:utf-8 -*-
# @Date      :2021/5/1
# @Author    :Maoxian
import time
from selenium import webdriver
from selenium.webdriver import ChromeOptions

options = ChromeOptions()
options.add_argument("--proxy-server=http://122.138.155.172:9999")

driver = webdriver.Chrome(options=options)
driver.get("http://httpbin.org/get")

time.sleep(2)

print(driver.page_source)

driver.quit()
