# -*- coding: utf-8 -*-
# @Date    : 2021/4/27
# @Author  : Maoxian

#
# 安装模块：pip install selenium
# 下载安装：selenium driver ，我使用的是 microsoft Edge 90.0.818.49
# 下载地址：https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/
import time
from selenium import webdriver
from lxml import etree


def get_one_page():
    # 获取一页的书本信息
    books_info = html.xpath('//*[@id="bookItem"]/a')
    for book in books_info:
        name = book.xpath('./div[2]/@title')
        price = book.xpath('./div[3]/div[1]/text()')
        print(f'书名：{name}，价格：{price}')

    # 点击下一页按钮
    time.sleep(0.5)
    edge.find_element_by_xpath('//button[@class="btn-next"]').click()


if __name__ == '__main__':

    edge = webdriver.Edge()  # 实例化一个Edge
    edge.get("http://www.epubit.com/books")  # 使用Edge访问网站

    for i in range(1, 11):
        page_source = edge.page_source  # 获取页面源码
        html = etree.HTML(page_source)
        get_one_page()
        print(f'==== 第{i}页打印完毕. ====')
        time.sleep(1)

    edge.quit()
