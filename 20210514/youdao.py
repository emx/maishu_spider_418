# -*- coding: utf-8 -*-
# @Date    : 2021/5/15
# @Author  : Maoxian
# 有道翻译
# 使用逆向破解技术
#


from hashlib import md5
import time
import random
import requests

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0',
    'Referer': 'https://fanyi.youdao.com/',
    'Cookie': 'OUTFOX_SEARCH_USER_ID=308861758@10.169.0.84; '
              'JSESSIONID=aaay5CFpuKwdEf266AWLx; '
              'OUTFOX_SEARCH_USER_ID_NCOO=1187262013.637317; '
              '___rl__test__cookies=1621099929803'
}


def fanyi(word):
    url = "https://fanyi.youdao.com/translate_o?smartresult=dict&smartresult=rule"

    bv = md5(headers['User-Agent'].split('Mozilla/')[-1].encode()).hexdigest()
    lts = int(time.time() * 1000)
    salt = f'{lts}{int(random.random() * 10)}'
    sign = md5(f"fanyideskweb{word}{salt}Tbh5E8=q6U3EXe+&L[4c@".encode()).hexdigest()
    data = dict(
        i=word,
        salt=salt,
        sign=sign,
        lts=lts,
        bv=bv,
        client='fanyideskweb',
        smartresult='dict',
        doctype='json',
        version='2.1',
        keyfrom='fanyi.web',
        action='FY_BY_REALTlME'
    )

    res = requests.post(url, headers=headers, data=data, timeout=5)
    return res.json()


print(fanyi('data'))
