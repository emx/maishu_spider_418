# -*- coding: utf-8 -*-
# @Date    : 2021/4/26
# @Author  : Maoxian


# 需求：获取异步社区网站10页的书本信息（书名,价格）：
# 网址：https://www.epubit.com/books

import requests

headers = {
    "Origin-Domain": "www.epubit.com",
    "X-Requested-With": "XMLHttpRequest",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) "
                  "Chrome/90.0.4430.85 Safari/537.36 Edg/90.0.818.46"
}

# 分析得到准确的数据地址
get_book_url = "https://www.epubit.com/pubcloud/content/front/portal/getUbookList"

for page in range(1, 10):
    params = dict(
        page=page,
        row=20,
        startPrice=None,
        endPrice=None,
        tagId=None
    )

    response = requests.get(get_book_url, params=params, headers=headers)
    books = response.json()['data']['records']

    for book in books:
        print(f'书名：{book["name"]}  价格：{book["price"]}')
