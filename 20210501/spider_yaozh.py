# -*- coding:utf-8 -*-
# @Date      :2021/5/1
# @Author    :Maoxian

# 使用代理云的ip池爬取药智数据网站的医院信息
import requests
from dailiyun import get_ip


class YaozhSpyder:

    def __init__(self, num):
        # 构造函数传入爬取数量
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0'
        }
        self.base_url = 'https://db.yaozh.com/hmap'
        self.num = num
        self.proxy_list = [{'https': f'https://{ip}'} for ip in get_ip(count=self.num)]

    @property
    def get_links(self):
        links = [self.base_url + f'/{_}.html' for _ in range(1, self.num + 1)]
        return links

    def get_data(self, url, proxy):
        res = requests.get(url, headers=self.headers, proxies=proxy,
                           allow_redirects=False)
        print('当前请求使用的proxy：', proxy)
        print('当前请求的链接：', res.url)
        print('当前返回的响应码：', res.status_code)
        print('================================================')


if __name__ == '__main__':
    spyder = YaozhSpyder(10)
    for link in spyder.get_links:
        spyder.get_data(link, proxy=spyder.proxy_list.pop())
