# -*- coding:utf-8 -*-
# @Date      :2021/5/1
# @Author    :Maoxian


# 代理云API
import requests


def get_ip(count=1, rand="false", norepeat="false", word=None, detail="false", ltime=0):
    """
    count: 数量
    rand: 是否随机
    norepeat: 当日去重
    word: 筛选地区
    detail: 显示详细信息
    ltime: 时效
    """
    api = "http://imaoxian.v4.dailiyun.com/query.txt"
    params = {
        "key": "NPD9BD2BA1",
        "word": word,
        "count": count,
        "rand": rand,
        "ltime": ltime,
        "norepeat": norepeat,
        "detail": detail
    }
    res = requests.get(api, params=params)
    ip_list = res.text.strip().split('\r\n')
    return ip_list


if __name__ == '__main__':
    print(get_ip(5))
