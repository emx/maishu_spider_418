# -*- coding:utf-8 -*-
# @Date      :2021/5/5
# @Author    :Maoxian

# 使用aiohttp 协程实现上次的项目

import os
import time
import asyncio
import aiohttp
import requests
from lxml import etree

base_url = "https://sc.chinaz.com/jianli/free.html"
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0'
}

response = requests.get(base_url, headers=headers)
html = etree.HTML(response.text)
page_links = ['https:' + i for i in html.xpath('//*[@id="container"]/div/a/@href')]  # 获取所有简历模板的页面链接


async def download(link):
    conn = aiohttp.TCPConnector(verify_ssl=False)
    async with aiohttp.ClientSession(connector=conn, headers=headers) as session:
        async with session.get(link) as r1:
            text = await r1.read()
            page_html = etree.HTML(text.decode('utf8'))
            down_title = page_html.xpath('//div[@class="ppt_tit clearfix"]/h1/text()')[0]  # 获取简历模板名称
            down_title = down_title.strip().replace('下载', '')
            down_link = page_html.xpath('//*[@id="down"]/div[2]/ul/li[1]/a/@href')[0]  # 获取下载链接
            down_suffix = down_link.split('.')[-1]  # 获取文件后缀

            async with session.get(down_link) as r2:
                print(f'《{down_title}》开始下载')
                content = await r2.read()
                with open(f'files/{down_title}.{down_suffix}', 'wb') as f:  # 保存文件内容
                    f.write(content)
                    print(f'《{down_title}》下载完成')


async def main():
    tasks = []
    for link in page_links:
        t = asyncio.create_task(download(link))
        tasks.append(t)

    for t in tasks:
        await t


if __name__ == '__main__':
    if not os.path.exists('files'):  # 创建文件夹
        os.mkdir('files')

    start = time.time()
    # asyncio.run(main()) # 会报错：RuntimeError: Event loop is closed
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())

    end = time.time()
    print(f"程序共耗时：{end - start}")
