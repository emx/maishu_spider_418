# -*- coding:utf-8 -*-
# @Date      :2021/5/5
# @Author    :Maoxian
import time
import asyncio


async def hello(num):
    print(num, time.time())
    await asyncio.sleep(1)
    print(num, time.time())


async def run():
    tasks = []
    for i in range(5):
        t = asyncio.create_task(hello(i))
        tasks.append(t)
    for t in tasks:
        await t


if __name__ == '__main__':
    start = time.time()
    asyncio.run(run())
    end = time.time()
    print(f"程序耗时：{end - start}")
