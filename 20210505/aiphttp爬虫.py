# -*- coding:utf-8 -*-
# @Date      :2021/5/5
# @Author    :Maoxian

# aiohttp 是使用协程实现的异步http库


from aiohttp import ClientSession
import asyncio

url = 'http://www.baidu.com'
urls = [url, url, url, url, url]


async def hello(url):
    async with ClientSession() as session:
        async with session.get(url) as response:
            print(response.status)


async def get_all():
    tasks = []
    for u in urls:
        t = asyncio.create_task(hello(u))
        tasks.append(t)
    for t in tasks:
        await t


if __name__ == '__main__':
    asyncio.run(get_all())
