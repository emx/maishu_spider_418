# -*- coding: utf-8 -*-
# @Date    : 2021/4/29
# @Author  : Maoxian
# 联众开发文档：https://www.jsdati.com/docs/guide
import json
import requests


class LianZhong:
    """联众答题的类, APIv2"""

    def __init__(self, username, password):
        self.headers = {
            'Host': 'v2-api.jsdama.com',
            'Connection': 'keep-alive',
            'Content-Length': '298',
            'Content-Type': 'text/json',
            'Accept': 'application/json, text/javascript, */*; q=0.01',
            'Accept-Language': 'zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3',
            'Accept-Encoding': 'gzip, deflate',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0'
        }
        self.username = username
        self.password = password
        self.soft_id = 24890
        self.soft_secret = 'ONVmNjD4RU4CNj7oea4Mx6LWrQ4HXRR5cJg5oIFo'

    def upload(self, img, img_type=1001, min_length=0, max_length=0):
        """上传图片并返回结果
        param img: 图片内容的base64结果
        param img_type: 验证码类型，参考 https://www.jsdati.com/docs/price
        param min_length: 识别的最小长度, 1个字母或汉字都计长度1
        param max_length: 识别的最大长度
        """
        api_url = "https://v2-api.jsdama.com/upload"
        body = {
            "softwareId": self.soft_id,
            "softwareSecret": self.soft_secret,
            "username": self.username,
            "password": self.password,
            "captchaData": img,
            "captchaType": img_type,
            "captchaMinLength": min_length,
            "captchaMaxLength": max_length,
            "workerTipsId": 0
        }

        rs = requests.Session()
        response = rs.post(api_url, headers=self.headers, data=json.dumps(body))
        return response.json()

    def check_points(self):
        """查看剩余点数"""
        api = "https://v2-api.jsdama.com/check-points"
        body = {
            "softwareId": self.soft_id,
            "softwareSecret": self.soft_secret,
            "username": self.username,
            "password": self.password
        }
        response = requests.post(api, headers=self.headers, data=json.dumps(body))
        return response.json()
