# -*- coding:utf-8 -*-
# @Date      :2021/5/2
# @Author    :Maoxian
import base64
import requests
from lianzhong import LianZhong

username = "186xxxxxxxx"
password = "xxxxxxxx"
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0'
}

# 获取验证码图片
rs = requests.Session()
code_img = rs.get("https://www.okcis.cn/php/checkUser/code.php", headers=headers).content

# 以下2种方式 二选一
# ###########  手动输入验证码  ###############
# 下载验证码图片
with open('code_tmp.jpg', 'wb') as f:
    f.write(code_img)
code = input("请输入验证码: ")
# #########################################


# ###########  使用联众自动答题  #############
# code_img_base64 = base64.b64encode(code_img).decode()
# lz = LianZhong('xxxxxxxx', 'xxxxxxxx')
# lz_res = lz.upload(code_img_base64, img_type=1201)
# code = lz_res['data']['recognition']
# # print(code)  # 查看验证码
# #########################################


# 构造登录参数
data = {
    "info[uname]": username,
    "info[pwd]": password,
    "info[yzm]": code,
    "info[jzmm]": 1,
    "Submit": "%BF%EC%CB%D9%B5%C7%C2%BC"

}
res = rs.post("https://www.okcis.cn/signed/", headers=headers, data=data)

# 判断是否登录成功
if '我的办公室首页' in res.text:
    print('登录成功！')
    with open('index.html', 'w') as f:
        f.write(res.text)
else:
    print('登录失败！')
