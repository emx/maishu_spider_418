# -*- coding: utf-8 -*-
# @Date    : 2021/4/19
# @Author  : Maoxian


import requests

from lxml import etree

url = 'https://movie.douban.com/top250'
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.72 Safari/537.36 Edg/90.0.818.39'
}

r = requests.get(url, headers=headers)

item_tree = etree.HTML(r.text)

l = item_tree.xpath('//div[@class="item"]')
print(l)