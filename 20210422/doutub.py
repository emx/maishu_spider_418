# -*- coding: utf-8 -*-
# @Date    : 2021/4/22
# @Author  : Maoxian

import os
import requests
from lxml import etree

headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.72 Safari/537.36 Edg/90.0.818.42"
}
base_url = "https://www.doutub.com/"
r = requests.get(base_url).text
tree = etree.HTML(r)
div_list = tree.xpath('//div[@class="new-list clearfix"]/div')


def download_pic(url, pic_name, pic_type, save_path='./doutub1'):
    if not os.path.exists(save_path):
        os.makedirs(save_path)

    pic = requests.get(url).content

    with open(f'{save_path}/{pic_name}.{pic_type}', 'wb') as f:
        f.write(pic)
        print(f'{pic_name} 下载完成.')


for d in div_list:
    pic_name = d.xpath('./a/span/text()')[0]
    pic_url = d.xpath('./a/img/@src')[0]
    pic_type = pic_url.split('.')[-1]

    download_pic(pic_url, pic_name, pic_type)
