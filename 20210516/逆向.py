# -*- coding: utf-8 -*-
# @Date    : 2021/5/16
# @Author  : Maoxian

# 网站: http://jzsc.mohurd.gov.cn/data/company
# 通过分析要获取数据的url为:
# http://jzsc.mohurd.gov.cn/api/webApi/dataservice/query/comp/list?pg=0&pgsz=15&total=0
# http://jzsc.mohurd.gov.cn/api/webApi/dataservice/query/comp/list?pg=1&pgsz=15&total=450
# http://jzsc.mohurd.gov.cn/api/webApi/dataservice/query/comp/list?pg=2&pgsz=15&total=450

# 通过在js中搜索常用加密解密算法:base64、md5、sha1、hmac、des、aes
# 通过搜索AES或Base64得到算法代码，其中获取 mode->mode, iv->m, key->f, text->n->t

"""找到相关的js 代码
f = d.a.enc.Utf8.parse("jo8j9wGw%6HbxfFn")
m = d.a.enc.Utf8.parse("0123456789ABCDEF")
function h(t) {
    var e = d.a.enc.Hex.parse(t),          // 第一层:Hex解析，将密文转为为正常十六进制的bytes， python: a2b_hex(t.encode('utf8'))
        n = d.a.enc.Base64.stringify(e),   // 第二层:base64 编码，将bytes编码为base64，没弄明白为什么python中为什么不需要这个步骤，而是用上面hex的text解密
        a = d.a.AES.decrypt(n, f, {        // 第三层:AES解密
        iv: m,
        mode: d.a.mode.CBC,
        padding: d.a.pad.Pkcs7
    }),
    r = a.toString(d.a.enc.Utf8);
    return r.toString()
}
"""
import json
import re
import time
import requests
from binascii import a2b_hex
from Crypto.Cipher import AES

rs = requests.Session()
rs.headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0',
    'Referer': 'http://jzsc.mohurd.gov.cn/data/company'
}


def get_res(page):
    url = f'http://jzsc.mohurd.gov.cn/api/webApi/dataservice/query/comp/list?pg={page}&pgsz=15&total=450'
    res = rs.get(url)

    key = "jo8j9wGw%6HbxfFn"
    iv = "0123456789ABCDEF"
    result = decrypt_aes(res.text, key, iv=iv)

    # 去除 ascii 控制字符(0-31,127)
    control_char = [chr(_) for _ in range(32)]
    control_char.append(chr(127))
    result = re.sub('|'.join(control_char), '', result)
    try:
        data = json.loads(result.strip())
        if data["code"] == 200:
            for item in data["data"]["list"]:
                # {'QY_FR_NAME': '廖露云', 'QY_REGION': '360500', 'QY_NAME': '江西天工水陆工程设计有限公司',
                # 'QY_REGION_NAME': '江西省-新余市', 'QY_ORG_CODE': '91360500079012847X',
                # 'COLLECT_TIME': 1477106416000, 'RN': 1, 'QY_ID': '001607220057194391',
                # 'QY_SRC_TYPE': '0', 'OLD_CODE': '079012847'}
                print('#' * 60)
                print('企业名称:', item['QY_NAME'])  # 企业名称
                print('企业法人:', item['QY_FR_NAME'])  # 企业法人
                print('所在地区:', item['QY_REGION_NAME'])  # 地区
                print('企业代码:', item['QY_ORG_CODE'])  # 企业代码
                # print('企业id:', item['QY_ID'])  # 企业id
                print('收集时间:', time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(item['COLLECT_TIME'] / 1000)))
    except json.decoder.JSONDecodeError:
        print(f"Error: json解码失败: {result}")


def decrypt_aes(text, key, mode=AES.MODE_CBC, iv=None):
    hex_text = a2b_hex(text.encode('utf8'))  # 将原始密文转成16进制格式的密文

    cipher = AES.new(key.encode(), mode, iv.encode())  # 根据key, mode, iv 生成钥匙
    text_decrypted = cipher.decrypt(hex_text)  # 使用钥匙解密
    return text_decrypted.decode('utf8')  # 转成字符串


if __name__ == '__main__':
    for page in range(10):
        get_res(page)
        time.sleep(1)
